package Devices;

import mysql.Conector;

public class Devices {
	private int deviceId;
	private String name;
	private String description;
	private int manufacturerId;
	private int colorId;
	private String comments;
	
	public Devices(int deviceId,String name, String description, int manufacturerId, int colorId,
			String comments) {
		this.name = name;
		this.deviceId = deviceId;
		this.description = description;
		this.manufacturerId = manufacturerId;
		this.colorId = colorId;
		this.comments = comments;
	}
	
	public Devices() {}

	@Override
	public String toString() {
		return "Devices [deviceId=" + deviceId + ", name=" + name + ", description=" + description + ", manufacturerId="
				+ manufacturerId + ", colorId=" + colorId + ", comments=" + comments + "]";
	}

	public int getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(int deviceId) {
		this.deviceId = deviceId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getManufacturerId() {
		return manufacturerId;
	}

	public void setManufacturerId(int manufacturerId) {
		this.manufacturerId = manufacturerId;
	}

	public int getColorId() {
		return colorId;
	}

	public void setColorId(int colorId) {
		this.colorId = colorId;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	};
	
	public Devices getDevice() {
		return this;
	}
	
}
