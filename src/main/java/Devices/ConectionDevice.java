package Devices;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import mysql.Conector;

public class ConectionDevice {
	// auto close connection
    Connection conn;
    
    public ConectionDevice(){}
    
    public List<Devices> getAll(){
    	try (Connection conn = DriverManager.getConnection(
	            "jdbc:mysql://127.0.0.1:3306/session3?serverTimezone=UTC#", "root", "1234")) {
    	
    		String SQL_SELECT = "Select * from DEVICE";
	    	PreparedStatement preparedStatement = conn.prepareStatement(SQL_SELECT);
	        ResultSet resultSet = preparedStatement.executeQuery();
	        
	        List<Devices> devices = new ArrayList();
	
	        while (resultSet.next()) {
	        	devices.add(new Devices(
	        			resultSet.getInt("deviceId"),
	        			resultSet.getString("name"),
	        			resultSet.getString("description"),
	        			resultSet.getInt("manufacturerId"),
	        			resultSet.getInt("colorId"),
	        			resultSet.getString("comments"))
	        			);
	        }
	        return devices;
    	}catch(SQLException e) {
    		System.out.println(e);
    		return null;
    	}
    }
    
}
