package main;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import Devices.ConectionDevice;
import Devices.Devices;
import mysql.Conector;
import static java.util.stream.Collectors.*;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ConectionDevice conexion = new ConectionDevice();
		List<Devices> dispositivos = conexion.getAll();
		
		List<String> nombres = dispositivos.stream()
								.map((devices)->devices.getName())
								.filter((names)->{
									if(names.equals("Laptop"))
										return true;
									else
										return false;
								})
								.collect(Collectors.toList());
		System.out.println("\n\nDispositivos laptops");
		nombres.forEach(System.out::println);
		
		Long numberOfId3Devices = dispositivos.stream()
					.filter((device)->{
						if(device.getDeviceId() == 3)
							return true;
						else
							return false;
					})
					.count();
		System.out.println("\n\nDispositivos con id  = 3");

		System.out.println(numberOfId3Devices);
		
		List<Devices> dispositivosColor1 = dispositivos.stream()
											.filter((dispositivo)->{
												if(dispositivo.getColorId() == 1)
													return true;
												else
													return false;
											})
											.collect(Collectors.toList());
		System.out.println("\n\nDispositivos con id color = 1");
		dispositivosColor1.forEach(System.out::println);
		
		Map<Integer, Devices> mapaDevices = dispositivos.stream()
									.collect(Collectors.toMap(Devices::getDeviceId,Devices::getDevice));
		System.out.println("\n\nMapa de los dispositivos con la llave como id y valor el objeto");
		System.out.println(mapaDevices);
	}

}
